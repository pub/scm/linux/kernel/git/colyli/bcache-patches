From e1f37c78f682ca8d7d0dee51ee8a0ee884f92df5 Mon Sep 17 00:00:00 2001
From: Coly Li <colyli@suse.de>
Date: Sun, 12 Dec 2021 23:13:09 +0800
Subject: [PATCH v13 00/12] bcache for 5.17: enable NVDIMM for bcache journal

Hi Jens,

This is the v12 effort the enabling NVDIMM for bcache journal, the code
is under testing for months and quite stable now. Please consider to
take them for Linux v5.17 merge window.

All current code logic and on-media format are consistent with previous
v12 series. The major difference from v12 series include,
- more typos in code comments and commit logs are fixed.
- add kernel message to indicate only first range is used currently if
  the NVDIMM namespace has multiple mapping ranges.
- not export nvm-pages allocator APIs, it is unnecessary since currently 
  only bcache uses them.

Now all previous bcache related UAPI headers are all moved into bcache
private code directory, there is no global headers exported to neither
kernel or user source code.

Bcache uses nvm-pages allocator to allocate pages from NVDIMM namespace
for its journaling space. The nvm-pages allocator is a buddy-like
allocator, which allocates size in power-of-2 pages from the NVDIMM
namespace. User space tool 'bcache' has a new added '-M' option to
format a NVDIMM namespace and register it via sysfs interface as a
bcache meta device. The nvm-pages allocator code does a DAX mapping to
map the whole namespace into system's memory address range, and allocate
the pages to requestion like typical buddy allocator does. The major
difference is nvm-pages allocator maintains the pages allocated to each
requester by an allocation list which stored on NVDIMM too. Allocation
list of different requester is tracked by a pre-defined UUID, all the
pages tracked in all allocation lists are treated as allocated busy
pages and won't be initialized into buddy system after the system
reboots.

The bcache journal code may request a block of power-of-2 size pages
from the nvm-pages allocator, normally it is a range of 256MB or 512MB
continuous pages range. During meta data journaling, the in-memory jsets
go into the calculated nvdimm pages location by kernel memcpy routine.
So the journaling I/Os won't go into block device (e.g. SSD) anymore,
the write and read for journal jsets happen on NVDIMM. 

Intel developers Jianpeng Ma and Qiaowei Ren compose the initial code of
nvm-pages allocator, the related patches are,
- bcache: initialize the nvm-pages allocator
- bcache: initialization of the buddy
- bcache: bch_nvm_alloc_pages() of the buddy
- bcache: bch_nvm_free_pages() of the buddy
- bcache: get recs list head for allocated pages by specific uuid
All the code depends on Linux libnvdimm and dax drivers, the bcache nvm-
pages allocator can be treated as user of these two drivers.

I modify the bcache code to recognize the nvm meta device feature,
initialize journal on NVDIMM, and do journal I/Os on NVDIMM in the
following patches,
- bcache: add initial data structures for nvm pages
- bcache: use bucket index to set GC_MARK_METADATA for journal buckets
  in bch_btree_gc_finish()
- bcache: add BCH_FEATURE_INCOMPAT_NVDIMM_META into incompat feature set
- bcache: initialize bcache journal for NVDIMM meta device
- bcache: support storing bcache journal into NVDIMM meta device
- bcache: read jset from NVDIMM pages for journal replay
- bcache: add sysfs interface register_nvdimm_meta to register NVDIMM
  meta device

All the code is EXPERIMENTAL, they won't be enabled by default until we
feel the NVDIMM support is completed and stable. The current code has
been tested internally for monthes, we don't observe any issue during
all tests with or without enabling the configuration.

Please consider to pick this series for Linux v5.17 merge window. If
there is any issue detected, we will response in time and fix them ASAP.

Thank you in advance.

Coly Li

Cc: Christoph Hellwig <hch@lst.de>
Cc: Dan Williams <dan.j.williams@intel.com>
Cc: Hannes Reinecke <hare@suse.de>
Cc: Jens Axboe <axboe@kernel.dk>
Cc: Jianpeng Ma <jianpeng.ma@intel.com>
Cc: Qiaowei Ren <qiaowei.ren@intel.com>
Cc: Ying Huang <ying.huang@intel.com>
---

Coly Li (7):
  bcache: add initial data structures for nvm pages
  bcache: use bucket index to set GC_MARK_METADATA for journal buckets
    in bch_btree_gc_finish()
  bcache: add BCH_FEATURE_INCOMPAT_NVDIMM_META into incompat feature set
  bcache: initialize bcache journal for NVDIMM meta device
  bcache: support storing bcache journal into NVDIMM meta device
  bcache: read jset from NVDIMM pages for journal replay
  bcache: add sysfs interface register_nvdimm_meta to register NVDIMM
    meta device

Jianpeng Ma (5):
  bcache: initialize the nvm pages allocator
  bcache: initialization of the buddy
  bcache: bch_nvmpg_alloc_pages() of the buddy
  bcache: bch_nvmpg_free_pages() of the buddy allocator
  bcache: get recs list head for allocated pages by specific uuid

 drivers/md/bcache/Kconfig        |  10 +
 drivers/md/bcache/Makefile       |   1 +
 drivers/md/bcache/btree.c        |   6 +-
 drivers/md/bcache/features.h     |   9 +
 drivers/md/bcache/journal.c      | 321 +++++++++--
 drivers/md/bcache/journal.h      |   2 +-
 drivers/md/bcache/nvmpg.c        | 931 +++++++++++++++++++++++++++++++
 drivers/md/bcache/nvmpg.h        | 128 +++++
 drivers/md/bcache/nvmpg_format.h | 253 +++++++++
 drivers/md/bcache/super.c        |  53 +-
 10 files changed, 1646 insertions(+), 68 deletions(-)
 create mode 100644 drivers/md/bcache/nvmpg.c
 create mode 100644 drivers/md/bcache/nvmpg.h
 create mode 100644 drivers/md/bcache/nvmpg_format.h

-- 
2.31.1

